#include "unity.h"

#include "LED.h"
#include "mock_sapi_gpio.h"

void setUp(void)
{
	gpioWrite_IgnoreAndReturn(0);
	gpioToggle_IgnoreAndReturn(0);
}

void tearDown(void)
{
}

void test_LED_toggle(void)
{
	LED_t led;
	toggle(&led);
}

void test_LED_on(void)
{
	LED_t led;
	on(&led);
}

void test_LED_off(void)
{
	LED_t led;
	off(&led);
}
